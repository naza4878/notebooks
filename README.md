# *Jupyter notebooks* de apoyo a cursos de ciencias básicas
Para descargar y ejecutar localmente o en línea con Binder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/japosada%2Fnotebooks/master)

**Advertencia:** el lanzamiento inicial del recurso en Binder puede demorar un par de minutos mientras se generan o cargan contenedores o máquinas virtuales en la nube de Binder. Tener paciencia al respecto.

Los cuadernillos `Tutorial_Python_A.ipynb` y `Tutorial_Python_B.ipynb` presenta material introductorio de `Python`. En el primero se describe la sintaxis básica y se presentan algunas estructuras de control. En el segundo se tienen estructuras básicas de datos y ejemplos de módulos comunes en ciencias como es el caso de `numpy`, `matplotlib` y `sympy`. En ambos cuadernillos se cuenta con ejercicios de práctica para afianzar lo aprendido.

En cada uno de los subdirectorios se irán publicando cuadernillos de apoyo, y a manenra de ejemplo, en la carpeta `MNUM`, se tienen algoritmos básicos correspondientes a un curso de métodos numéricos.
